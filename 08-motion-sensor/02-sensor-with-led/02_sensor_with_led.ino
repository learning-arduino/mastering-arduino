#include <Arduino.h>
#include "User_Setup.h"

#define MOTION_SENSOR 3
#define LED_PIN 5

void setup() {
    pinMode(MOTION_SENSOR, INPUT);
    pinMode(LED_PIN, OUTPUT);

    digitalWrite(LED_PIN, LOW);
    Serial.begin(9600);
}

void loop() {
    int sensorValue = digitalRead(MOTION_SENSOR);
    if (sensorValue == HIGH) {
        Serial.println("Motion detected");
    }
    digitalWrite(LED_PIN, sensorValue);
    delay(500);
}
