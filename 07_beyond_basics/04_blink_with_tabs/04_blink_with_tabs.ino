#include "led.h"

void setup() {
  pinMode(LED_ONE, OUTPUT);
  pinMode(LED_TWO, OUTPUT);
}

void loop() {
  blink_led(LED_ONE);
  delay(1000);
  blink_led(LED_TWO);
}
