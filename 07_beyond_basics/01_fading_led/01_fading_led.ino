#define LED_ONE 11

int val = 0;
int change = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_ONE, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  val += change;
  if (val > 250 || val < 5) {
    change *= -1;
  }
  analogWrite(LED_ONE, val);
  delay(100);
}
